import got from './data-1.js'

function countAllpeople()
{
    let count=0;
    for(let house of got.houses)
    {        
            count+=house.people.length   
    }
    return count;
}

const count=countAllpeople();
// console.log(count);

function everyone(){
const newarr=[];
for(let house of got.houses)
{
    for(let people of house.people)
    {
        newarr.push(people.name);
    }
}
return newarr;
}

const res=everyone();
// console.log(res);



function namewithS()
{
    const newarr=[];
    for(let house of got.houses)
    {
        for(let people of house.people){
            if(people.name.startsWith("s") || people.name.startsWith("S"))
            {
                newarr.push(people.name);
            }
        }
    }
    return newarr;

}

const newarr=namewithS();
// console.log(newarr);


function namewithA()
{
    const newarr=[];
    for(let house of got.houses)
    {
        for(let people of house.people){
            if(people.name.startsWith("a") || people.name.startsWith("A"))
            {
                newarr.push(people.name);
            }
        }
    }
    return newarr;

}

const arr=namewithA();
// console.log(arr);



function surnameWithS()
{
    let newarr=[];
    for(let house of got.houses)
    {
        for(let people of house.people)
        {
            let lname=people.name.split(" ").pop();
            if(lname.startsWith("s") || lname.startsWith("S"))
            {
                newarr.push(people.name);
            }
        }
    }
    return newarr;
}


const sarray=surnameWithS();
// console.log(sarray);


function surnameWithA()
{
    let newarr=[];
    for(let house of got.houses)
    {
        for(let people of house.people)
        {
            let lname=people.name.split(" ").pop();
            if(lname.startsWith("a") || lname.startsWith("A"))
            {
                newarr.push(people.name);
            }
        }
    }
    return newarr;
}


const Aarray=surnameWithA();
console.log(Aarray);

